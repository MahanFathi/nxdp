# NxDP
Implementation of the NxDP method describe in the report

## Run
This implementation takes advantage of JAX and can be run on CPUs, GPUs or TPUs (without modification). 
### Local
#### Setup
To install all the required package (we recommend you create a dedicated python environment):
```pip install -r requirements.txt```.

#### Run
You can run the code on your local machine with.
```python main.py```
The default configuration can be found in [config/default.py](config/default.py).
If you want to change some argument, you can run :
```python main.py --opts <arg1name> <arg1value> ... <argNname> <argNvalue>```
(standard hyperparameters setting for different environments are given in eponym YANL files in [config](config). )

### Colab
You can benefit from the free TPUs offer by Colab and run this notebook.
[Colab](https://colab.research.google.com/drive/1ire7TaEOeLVQ18KyQ7s235quV825zhai?usp=sharing)


## Resultat
### Logs
The tensorboard logs of the experiment presented in the report are here [Log](../data/).
### Visualization
The model have been finetuned in ant, halfcheetah and hopper BRAX environments. You can visualize the resulting policy in [Colab](https://colab.research.google.com/drive/1e2MTfRTH986brhxS_c1zryVp6gemJqRa?usp=sharing).
